docker kill price-service article-service frontend-service
docker rm price-service article-service frontend-service
docker network rm article-network
docker network rm price-network

docker network create article-network
docker network create price-network
docker run --name price-service --network price-network --network-alias=price-service -e "PORT=3000" -d price-service:1
docker run --name article-service --network article-network --network-alias=article-service -e "PORT=3001" -d article-service:1
docker run --name frontend-service --network article-network -p "8080:8080" -e "ARTICLE_SERVICE_HOST=http://article-service:3001" -e "PRICE_SERVICE_HOST=http://price-service:3000" -d frontend-service:1
docker network connect price-network frontend-service

